#include <iostream>

using namespace std;

void typeOfSequence(int number);

int main()
{
    int number;
    cout << "Enter number: ";
    cin >> number;
    typeOfSequence(number);
}

void typeOfSequence(int number)
{
    bool more = false;
    bool less = false;
    bool equals = false;

   while (true)
    {
        int currentDigit = number % 10;
        number /= 10;
        int nextDigit = number % 10;
        cout << currentDigit << " " << nextDigit << endl;
        if (nextDigit != 0)
        {
            if (nextDigit > currentDigit)
            {
                more = true;
            }
            else if (nextDigit < currentDigit)
            {
                less = true;
            }
            else
            {
                equals = true;
            }
        }
        else
        {
            break;
        }
    }
    
    if (more && !less && !equals)
    {
        cout << "This is strictly descending sequence" << endl;
    }
    else if (more && !less && equals)
    {
        cout << "This is not strictly descending sequence" << endl;
    }
    else if (!more && less && !equals)
    {
        cout << "This is strictly increasing sequence" << endl;
    }
    else if (!more && less && equals)
    {
        cout << "This is not strictly increasing sequence" << endl;
    }
    else if (!more && !less && equals)
    {
        cout << "This is monotonic sequence" << endl;
    }
    else
    {
        cout << "This is not ordered sequence" << endl;
    }
}
