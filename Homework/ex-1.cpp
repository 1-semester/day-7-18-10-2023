#include <iostream>

using namespace std;

bool isPrime(int number);
void testNumber(int number);

int main()
{
    int number;
    cout << "Enter number: ";
    cin >> number;
    testNumber(number);
}

bool isPrime(int number)
{
    if (number <= 1)
    {
        return false;
    }

    for (int divider = 2; sqrt(number) >= divider; divider++)
    {
        if (number % divider == 0)
        {
            return false;
        }
    }
    return true;
}

void testNumber(int number)
{
    for (int i = 2; i <= number; i++)
    {
        if (isPrime(i))
        {
            cout << i << ", ";
        }
    }
}