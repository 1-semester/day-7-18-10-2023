#include <iostream>

using namespace std;

bool isPalindrome(long long number);
int getLenght(long long number);

int main()
{
    int number;
    cout << "Enter number: ";
    cin >> number;
    cout << number << " is ";
    if (isPalindrome(number))
    {
        cout << "palindrome" << endl;
    }
    else
    {
        cout << "not palindrome" << endl;
    }
}

int getLenght(long long number)
{
    int lenght = 0;
    while (number != 0)
    {
        number /= 10;
        lenght++;
    }
    return lenght;
}

bool isPalindrome(long long number)
{
    int lenght = getLenght(number);

    while (lenght > 2)
    {
        int first = number / pow(10, lenght - 1);
        int last = number % 10;
        if (first == last)
        {
            number = (number % (int)pow(10, lenght - 1)) / 10;
            lenght -= 2;
        }
        else
        {
            return false;
        }
    }
    return true;
}