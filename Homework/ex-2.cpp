#include <iostream>

using namespace std;

int deleteDigits(int number, int deletedDigit);

int main()
{
    int number;
    cout << "Enter number: ";
    cin >> number;
    int deletedDigit;
    cout << "Enter the number you want to delete: ";
    cin >> deletedDigit;
    cout << deleteDigits(number, deletedDigit) << endl;
}

int deleteDigits(int number, int deletedDigit)
{
    int result = 0;
    int power = 0;

    while (number != 0)
    {
        int currentDigit = number % 10;
        number /= 10;
        if (currentDigit != deletedDigit)
        {
            result += currentDigit * pow(10, power);
            power++;
        }
    }
    return result;
}
